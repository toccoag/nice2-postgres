#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<EOF
    -- Default user and datbase for use with Nice
    --
    -- This user must NOT have any admin right, we want to make sure changesets
    -- work without them.
    CREATE ROLE nice WITH LOGIN PASSWORD 'nice';
    CREATE DATABASE nice WITH OWNER nice;
EOF
