FROM postgres:15

# Override default user used by psql
ENV PGUSER postgres

# enable trust authentication
ENV POSTGRES_HOST_AUTH_METHOD trust

# default folder is a volume, thus in needs to be changed
ENV PGDATA /data

# add custom configuration
COPY config/* /etc/postgresql.d/

# add custom startup scripts
COPY scripts/* /docker-entrypoint-initdb.d/

# Override some commands
ENV PATH /nice-bin:$PATH
COPY bin/* nice-bin/
RUN chmod +x nice-bin/*
