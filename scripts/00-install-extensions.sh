#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname template1 <<EOF
    -- TODO remove once S3 support is mandatory / all installations are 3.8+
    CREATE EXTENSION lo;
    CREATE EXTENSION pg_trgm;
    CREATE EXTENSION "uuid-ossp";
    CREATE EXTENSION unaccent;
    
    -- Remove comments (COMMENT ON … IS NULL for all extensions)
    DELETE FROM pg_catalog.pg_description where classoid = 'pg_catalog.pg_extension'::pg_catalog.regclass;
EOF
