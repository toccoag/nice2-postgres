# Postgres for use with Nice2

There is one branch per Postgres version. Branches are automatically built
and Docker images are made available as:

```
registry.gitlab.com/toccoag/nice2-postgres:${BRANCH}
```


## User `nice2`

User for use in DB creation and migration as well as in integration tests.

The user has no admin right (=same access permissions as on prod databases).

Name: `nice`
Password: `nice`


## Databases

An empty database called `nice` is created with owner `nice`.


## About this Image

### Custom Postgres Configuration

Postgres configuratons can be changed by adding `/etc/postgresql.d/*.conf` files to the image. This files are automatically
included by the main `postgresql.conf` file.


### Running custom scripts

You can add custom scripts to `/docker-entrypoint-initdb.d/` to have them executed during the first start. Prefix them with `[1-9][0-9]-` to ensure they are run after the scripts added by this image.


### Updating Postgres

1. Check out most recent version/branch:

   ```
   git checkout ${BRANCH}
   git pull
   ```

2. Create new branch:

   ```
   git checkout -b ${POSTGRES_VERSION}
   ```

3. Update version in `Dockerfile`

4. Commit:

   ```
   git commit Dockerfile
   ```

5. Push branch:

   ```
   git push -u origin HEAD
   ```

6. [Schedule] weekly (re-)build

7. Go to [Protected branches] and protect ${BRANCH}:

   - Allowed to merge: DevOps (group)
   - Allowed to push: DevOps (group)


[Protected branches]: https://gitlab.com/toccoag/nice2-postgres/-/settings/repository#js-protected-branches-settings
[Schedule]: https://gitlab.com/toccoag/nice2-postgres/-/pipeline_schedules
